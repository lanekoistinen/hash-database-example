/////////////////////////////////////////////
//
//  LANE KOISTINEN
//  2.09.2020
//
//  Main calls  operations on a hash table
//  to help access a database of lisence 
//  plate
//  
////////////////////////////////////////////



#include "Header.h"



///////////////////////////////////////////
//	Main
//  Main reads a database and interperets
//  user commands
//  Inputs: the database and the table size
//  Ouptuts: 0 for success, 1 for error

int main(int argc, char **argv)
{
	// Declare default hash size
	printf("\n");
	int hashSize = 100;

	// Check for error in the program startup
	if (argc != 2 && argc != 3)
	{
		printf("Usage: %s database [size]\n\n", argv[0]);
		return 0;
	}
	
	// Set the hash table size if the user specifies a value
	if (argc == 3)
	{
		if (sscanf(argv[2], "%d", &hashSize) != 1)
		{
			// Administer an error if the user enters an invalid value
			if (hashSize < 1)
			{
				printf("Enter an integer greater than 0");
				return 1;
			}
			printf("Usage: Please type a valid integer\n\n");
			return 1;
		}		
	}

	// Declare the hash table with the appropriate size
	hashEntry* masterTable = hashInit(hashSize);

	// Open the file and read from the database
	FILE *fp = fopen(argv[1], "r");
	if (fp == NULL)
	{
		printf("File Opening Error\n\n");
		return 1;
	}
	
	char temp[256];
	while (fgets(temp, 256, fp) != NULL)
	{
		char plate[32], first[32], last[32];
		sscanf(temp, "%s %s %s", plate, first, last);

		// Add each plate from the database
		hashAdd(masterTable, plate, first, last, hashSize);	
		
	}
	fclose(fp);

	char *nullTest;
	do {
		printf("Enter a plate: ");

		// Declare variables
		int cmd = 3;
		int number = 0;
		char* testChar = malloc(130);
		char commandInput[100];

		nullTest = fgets(commandInput, 100, stdin);
		if (nullTest != NULL) // Stop if NULL input
		{
        		// Scan for all five different commands and adjust the
        		// result command according to findings
			sscanf(commandInput, "%s", testChar);
			int result = strcmp(testChar, "*LOAD");
			if (result == 0)
				cmd = 0;
			result = strcmp(testChar, "*DUMP");
			if (result == 0)
				cmd = 1;
			free(testChar);
        		result = sscanf(commandInput, "*DUMP %d", &number);
        		if (result == 1)
        	        	cmd = 2;
			
			// Issue command
			command(masterTable, cmd, number, hashSize, commandInput);
		}   
	} while (nullTest != NULL); // this indicates EOF is reached. Program is free to terminate
	
	// Free all memory
	hashFree(masterTable, hashSize);
	free(masterTable);
	printf("\nFreeing memory\n");
	return 0;
}




////////////////////////////////////////////////
//	command
// command takes the inputted command
// from main and calls the appropriate
// hash function
// Inputs: the specific command paramters, along
// with the hashtable and hashtable size
// Ouputs: the program returns 0 upon completion
//

int command(hashEntry* masterTable, int command, int number, int hashSize, char* input)
{
	char* first = malloc(130); 
	char* last = malloc(130);

	// Determine the input string length
	int len = strlen(input);

	// Determine the requested command
	switch (command)
	{
	// Load
	case 0: 
		hashLoad(masterTable, hashSize); 
	break;

	// Dump all cells
	case 1: 
		// Print each cell's linked list
		for (int ii = 0; ii < hashSize; ii++)
		{
			printf("-------------------------------------------\nContents of cell %d\n", ii);
			hashDump(masterTable, ii);
		}
	break;

	// Dump a specific cell
	case 2:
		// Only accept valid inputs
		if (number < hashSize && number >= 0)
		{
			printf("-------------------------------------------\n"
				"Contents of cell %d\n", number);
			hashDump(masterTable, number);
			printf("-------------------------------------------\n");
		}
		else 
		{
			printf("ERROR: cell must be between 0 and %d\n", hashSize - 1);
		}
	break;

	// Find a plate
	case 3:
		// Remove endlines from string
		if(input[len-1] == '\n')
			input[len-1] = 0;

		// Print if found
        	if (hashFind(masterTable, input, first, last, hashSize) == 1)
		{
		        printf("First name: %s\nLast name: %s\n", first, last);
			free(first); free(last);
			return 0;
		}
		printf("Plate not found\n");
	break;
	default:break;
	}
	// Free retrieval variables, first and last
	free(first); free(last);
	return 0;
}


