/////////////////////////////////////////////
//
//  LANE KOISTINEN
//  2.09.2020
//  Primary database Functions
//
//  Implemenation performs operations on a hash 
//  table to help access a database of lisence
//  plate
//
////////////////////////////////////////////



#include "Header.h"



////////////////////////////////////////
//	hashInit
// hash init initializes the cells of a 
// hash table
// Inputs: the hashsize
// Outputs: the table handle
//

hashEntry *hashInit(int hashsize)
{
	// Malloc space for the table
	hashEntry *table = malloc(hashsize * sizeof(hashEntry));	
	
	// Initialize all cells
	for(int ii = 0; ii < hashsize; ii++)
	{
		table[ii] = listInit();
	}
	return table;
}



////////////////////////////////////////
//	hashAdd
// hashAdd adds a new node to the appropriate
// linked list in the hash table
// Inputs: hashtable size, struct node parameters,
// and the hash table handle
// Output: none
//

void hashAdd(hashEntry *hashTable,
	char *plate,
	char *first,
	char *last,
	int hashTableSize)
{
	// Hash the plate to find the appropriate index
	int loc = hash(plate, hashTableSize);

	// Call the appropriate subfunction	
	listAdd(hashTable[loc], plate, first, last);
	return;
}

/////////////////////////////////////////
//	hashFind
// hashFind uses the hashed index to locate
// a node
// Inputs: The table handle, the struct parameters,
// and the table size
// Outputs: 1 for success, 0 for error
//

int hashFind(hashEntry *hashTable,
	char *plate,
	char *first,
	char *last,
	int hashTableSize)
{
	// Hash the plate and call the appropriate sub function
	int index = hash(plate, hashTableSize);	
	return listFind(hashTable[index],
		plate,
		first,
		last);
}




///////////////////////////////////////////////////
//	hashLoad
// hashLoad prints the contents of the table
// Inputs: the table handle and size
// Outputs: None
//

void hashLoad(hashEntry *hashTable, int hashTableSize)
{
	// Print all cells
	for (int ii = 0; ii < hashTableSize; ii++)
	{
		int x = listLen(hashTable[ii]);
		printf("Entry %d: length=%d \n", ii, x);
	} 
}



////////////////////////////////////////////////////
//	hashDump
// hashDump prints a cell of the table
// Inputs: the table and cell index
// Outputs: NOne
//

void hashDump(hashEntry *hashTable, int cellNum)
{
	listPrint(hashTable[cellNum]);
}



///////////////////////////////////////////////////
//	hashFree
// hashFree takes the table and calls the subfunction
// to free the memory of all linked lists
//
// Inputs: the table and the table size
// Outputs: none
//

void hashFree(hashEntry *hashTable, int hashTableSize)
{
	for( int ii = 0; ii < hashTableSize; ii++)
	{
		listFree(hashTable[ii]);
	}
	//free(hashTable);
}



////////////////////////////////////////////////////
//	hashIndex
// hashIndex returns an index for a given string
// Inputs: the plate string and the table size
// Outputs: an integer index
//

int hash(char *myString, int hashTableSize)
{
        int sum = 0;

	// Perform the hash calculation
        for (int ii = 0; ii < strlen(myString); ii++)
        {
                sum = sum + (ii + 5) * (int)myString[ii];
        }
        return sum % hashTableSize;
}


