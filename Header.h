/////////////////////////////////////////////
//
//  LANE KOISTINEN
//  2.09.2020
//
//  Header for Hash Table
//
////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node{
	char *plate;
	char *first;
	char *last;
	struct node *next;
};

typedef struct node* hashEntry; 

int command(hashEntry *table, int cmd, int nmbr, int size, char* input);

int hash(char *myString, int hashTableSize);


hashEntry *hashInit(int hashsize);
void hashAdd(hashEntry *hashTable, char *plate, char *first, char *last, int hashTableSize);
int hashFind(hashEntry *hashTable, char *plate, char *first, char *last, int hashTableSize);
void hashLoad(hashEntry *hashTable, int hashTableSize); 


void hashDump(hashEntry *hashTable, int cellNum); 
void hashFree(hashEntry *hashTable, int hashTableSize);



struct node *listInit();
void listAdd(struct node *sent, char *plate, char *firstname, char *lastname); 
int listFind(struct node *sent, char *plate, char *firstname, char *lastname); 
int listLen(struct node *sent); 
void listPrint(struct node *sent);
void listFree(struct node *sent); 

struct node* get_node(char* newPlate, char* newFirst, char* newLast);
void release_node(struct node*ll);
