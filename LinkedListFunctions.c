/////////////////////////////////////////////
//
//  LANE KOISTINEN
//  2.09.2020
//  Low-level interal database operations
//
//  LinkedListFunctions provides the low level
//  functions to operate on the linked list
//
////////////////////////////////////////////

#include "Header.h"


////////////////////////////////////
//	listInit
//
// listInit initializes a sentinal node
// the appropriate pointers of NULL
// 
// inputs: none
// outputs: a handle to the sentinal
//

struct node *listInit()
{
	// Initialize sentinal node
	struct node* sentinal = malloc(sizeof(struct node));
	sentinal -> plate = NULL;
	sentinal -> first = NULL;
	sentinal -> last = NULL;
	sentinal -> next = NULL;
	
	// Return a pointer to the sentinal
	return sentinal;
}



/////////////////////////////////////
//	listAdd 
//
// listAdd allocates memory for a new node,
// creates the new node, sets the appropriate
// pointers so it is at the beginning of the
// list
//
// inputs: the relevant list and the
//  the values the user wishes to insert
// outputs: there are no outputs
//

void listAdd(struct node* sent, char *plate, char *firstname, char *lastname)
{
	// store the first nodes pointer
	struct node* firstNode = sent -> next;

	// retrieve a new node using get_node
	struct node* newNode = get_node(plate, firstname, lastname);
	
	// Return if there was no memory
	if(newNode == NULL)
	{
		printf("OUT OF MEMORY\n");
		return;
	}

	// Adjust the pointers of and before the first node
	newNode -> next = firstNode;
	sent -> next = newNode;
}



//////////////////////////////////
//	listFind
//
// listFind looks at each data element
// of the list and returns success
// if it finds the inputted value
//
// inputs: the relevant list and 
//  the plate the user intends to search
// outputs: 1 if the number is found,
//  zero if the number is not found
//

int listFind(struct node*sent, char *p, char *f, char *l)
{
	// Start at the sentinal node
	struct node* currNode = sent -> next;
	
	// Search for the node
	while (currNode != NULL)
	{
		// Terminate with success if we find it
		if ( 0 == strcmp(currNode -> plate, p))
		{
			strcpy(f,currNode->first);
			strcpy(l,currNode->last);
			return 1;
		}


		// Otherwise, increment our current node
		currNode = currNode -> next;
	}
	
	// Indicate a failure if we have not yet found it
	return 0;
}



/////////////////////////////////////////
//	listLen
// 
// listLen calculates the length of a
//  linked list
// 
// inputs: the relevant list
// outputs: the total length
//

int listLen(struct node* sent)
{
	int len = 0;
	struct node* curr = sent -> next;
	while (curr != NULL)
	{
		// Increment sum and current node
		len = len +1;
		curr = curr -> next;
	}
	return len;
}



////////////////////////////////////
//	listPrint
//
// print prints a linked list node by
// node
// 
// inputs: the relevant list
// outputs: none
//

void listPrint(struct node* sent)
{
	// Set the current node to our sentinal
	struct node* currNode = sent -> next;

	// Print the next node's data if the pointer is not null
	while(currNode != NULL)
	{
		printf("License: <%s>  Name: %s, %s \n", currNode->plate, currNode->last, currNode->first);
		currNode = currNode -> next;
	}
}



///////////////////////////////////////
//	get_node
//
// get_node allocates memory for a new
// node and sets the data value
//
// input: the new parameters inputted by 
// the database
// outputs: a handle to the node
//

struct node*  get_node(char* newPlate, char* newFirst, char* newLast)
{
	// Allocate memory for a new node
	struct node* newNode = malloc(sizeof(struct node));
	
	// Cancel if the malloc faild
	if (newNode != NULL)
	{
		// malloc space for all strings		
		newNode -> plate = malloc(strlen(newPlate) + 1);
		newNode -> first = malloc(strlen(newFirst) + 1);
		newNode -> last  = malloc(strlen(newLast) + 1);

		// Store the new string
		strcpy(newNode -> plate, newPlate);
		strcpy(newNode -> first, newFirst);
		strcpy(newNode -> last, newLast);
		
		// Return the pointer to the new node
		return newNode;
	} 
	else
	{
		// Already done in higher level function: printf("error with malloc\n");
		return NULL;
	}
}



///////////////////////////////////////
//	release_node
//
// release_node frees the memory using a
// pointer
//
// inputs: a pointer to the node to be
// deleted
// outputs: none
//

void release_node(struct node*ll)
{
	// Free the memory
	free(ll);
}



///////////////////////////////////////
//      freeMem
//
// freeMem takes a handle to the list
// and frees all nodes and the sentinal
//
// inputs: a pointer to the list
// outputs: none
//

void listFree(struct node* list)
{
	// Start at the first node
	struct node* current = list -> next;
	struct node* previous = list;
	
	// Otherwise, progress down the list,
	// deleting the previous node as you go
	while (current != NULL)
	{
		free(previous->plate);
		free(previous->first);
		free(previous->last);
		free(previous);
		previous = current;
		current = current -> next;
	}

	// Handle the edge case by deleting the final nodes
	free(previous->plate);
	free(previous->first);
	free(previous->last);
	free(previous);
	/*free(current->plate);
	free(current->first);
	free(current->last);
	free(current);*/
}

